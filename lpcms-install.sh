# #!/bin/bash
CYAN='\033[0;36m'
NC='\033[0m'
INSTALLER_DIR=$(pwd);
cat << "EOF"
    __    ____  ________  ________    __                     __
   / /   / __ \/ ____/  |/  / ___/   / /   ____  _________ _/ /
  / /   / /_/ / /   / /|_/ /\__ \   / /   / __ \/ ___/ __ `/ / 
 / /___/ ____/ /___/ /  / /___/ /  / /___/ /_/ / /__/ /_/ / /  
/_____/_/    \____/_/  /_//____/  /_____/\____/\___/\__,_/_/   
EOF
sleep 0.5;

function installDependency {
  echo "Checking if you have $1 installed.";
  sleep 0.3;
  if ! command -v $2 > /dev/null; then
    echo "\nLooks like you don't. No problem. \nI'll download it for you! $3\n";
    $4
  else 
    echo "\nLooks like you do. $3\n" ;
    sleep 0.3;
  fi
}
Install dependencies
installDependency "Homebrew" "brew" "Cheers! 🍺🍺🍺" "/usr/bin/ruby -e \"$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)\"";
installDependency "PHP 7.3" "php" "🐘" "brew install php@7.3 && brew link php@7.3 && php -v";
installDependency "Mariadb" "mariadb" "💻" "brew install mariadb";
installDependency "WP CLI" "wp" "📝" "brew install wp-cli";
installDependency "Composer" "composer" "🎶" "php composer-setup.php --install-dir=bin --filename=composer && mv composer.phar /usr/local/bin/composer";
export PATH=$PATH:~/.composer/vendor/bin;
installDependency "Laravel Valet" "valet" "🚗" "composer global require laravel/valet && valet install";
installDependency "htop" "htop" "💻" "brew install htop";

echo "Awesome! You now you have all the dependencies you need to run the LPCMS locally! 🎉 🙌"; 
sleep 0.5;
echo "Now let's setup the folder structure you'll need.";
sleep 0.5;


# Get local path names for installation
echo "\nWhere do you want this project to be installed in your User directory? ${CYAN}example:${NC}\"Sites\""?
read projectPath;

echo "\nWhat do you want to call this site (lowercase - this will be the url with .test at the end and the directory name)? ${CYAN}example:${NC}\"lpcms\"";
read LPCMS_DIR;

if [ ! -d "$HOME/$projectPath" ]; then
  echo "\nGenerating that directory.";
  mkdir $HOME/$projectPath;
  if [ ! -d "$HOME/$projectPath/$LPCMS_DIR" ]; then
    mkdir $HOME/$projectPath/$LPCMS_DIR;
  fi
  cd $HOME/$projectPath && valet park && cd $HOME/$projectPath/$LPCMS_DIR;
  pwd;
else
  if [ ! -d "$HOME/$projectPath/$LPCMS_DIR" ]; then
    mkdir $HOME/$projectPath/$LPCMS_DIR;
  fi
  echo "Your project is located here: $HOME/$projectPath/$LPCMS_DIR";
fi


# Install WordPress with WP Cli
echo "Do you want to install WordPress? ${CYAN}[Y/N]${NC}";
read answer;

  if [ "$answer" == "Y" ] || [ "$answer" = "Yes" ] || [ "$answer" = "y" ] || [ "$answer" = "yes" ]
    then
    echo "-------------------------";
    echo "👊 Let's do this! 👊";
    echo "-------------------------";
    sleep 0.5;
    cd $HOME/$projectPath/$LPCMS_DIR;
    echo "What's your dbname? ${CYAN}lpcms${NC}";
    read name;
    sleep 0.5;
    
    echo "What's your dbuser? ${CYAN}root${NC}";
    read user;
    sleep 0.5;
    
    echo "What's your dbpass? ${CYAN}[use your local mariadb pass, which is blank by default]${NC}";
    read pass;

    wp core download && wp config create --dbname="$name" --dbuser="$user" --dbpass="$pass";
    mysql -u $user -p $pass -e "CREATE DATABASE lpcms";
    mysql -u $user -p $pass -f lpcms < $INSTALLER_DIR/lpcms.sql;
    wp option update home "http://${LPCMS_DIR}.test"
    wp option update siteurl "http://${LPCMS_DIR}.test"
  else
    echo "-------------------------";
    echo "👨‍🌾 Ok, this is gonna be a manual process for you!";
    echo "You're gonna have to import the DB, plugins, etc. manually. Or you could just run this script again and let us do it for you!";
    echo "-------------------------";
  fi


# Clone LPCMS Repo
echo "Grabbing the repo...";
git clone git@bitbucket.org:randallreilly/wordpress-lpcms.git;
rm -r wp-content;
mv wordpress-lpcms wp-content;

sleep 0.3;
echo "Installing the plugins";
rm -r $HOME/$projectPath/$LPCMS_DIR/wp-content/plugins;
cp -r $INSTALLER_DIR/plugins $HOME/$projectPath/$LPCMS_DIR/wp-content/ && cp $INSTALLER_DIR/rr-config.php $HOME/$projectPath/$LPCMS_DIR;

echo "Composer install";
cd $HOME/$projectPath/$LPCMS_DIR/wp-content/themes/randall-reilly;
composer install;

echo "npm install"
npm install;

echo "Sweet! Now we need to link your test sites with Valet";
cd $HOME/$projectPath/LPCMS_DIR;
valet link donutclient;
valet link baconclient;
valet link creativeclient;

echo "Ready to see your site?! ${CYAN}[Y/N]${NC}"?
read answer;
if [ "$answer" == "Y" ] || [ "$answer" = "Yes" ] || [ "$answer" = "y" ] || [ "$answer" = "yes" ]
    then
    echo "-------------------------";
    echo "Hold onto your butt!";
    echo "-------------------------";
    open "http://${LPCMS_DIR}.test/wp-admin";
  else
    echo "-------------------------";
    echo "Ok, your site is ready! You can visit it at http://${LPCMS_DIR}.test/wp-admin";
    echo "-------------------------";
  fi

  exit 1;