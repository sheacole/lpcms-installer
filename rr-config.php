<?php
// ** MySQL settings for ppc_client_leads ** //
// production credentials commented to the side

define('LEADS_DB_NAME', 'ppc_client_leads');

/** MySQL database username for ppc_client_leads */
define('LEADS_DB_USER', 'root');

/** MySQL database password for ppc_client_leads */
define('LEADS_DB_PASSWORD', 'password');

/** MySQL hostname for ppc_client_leads */
define('LEADS_DB_HOST', 'mysql');